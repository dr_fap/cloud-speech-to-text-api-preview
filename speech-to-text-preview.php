<?php
require 'vendor/autoload.php';

use Google\Cloud\Speech\V1\{SpeechClient, RecognitionAudio, RecognitionConfig};
use Google\ApiCore\ValidationException;
use Google\ApiCore\ApiException;

// Путь к JSON-файлу с данными ключа доступа
$credentialsPath = './files/keys/example.json'; // TODO: FILEKEY

// Создание клиентского объекта SpeechClient с использованием ключа доступа
try {
    $client = new SpeechClient(['credentials' => $credentialsPath]);

    // Путь к аудиофайлу
    $audioFile = './files/media/путь_к_вашему_аудиофайлу.wav';  // TODO: FILENAME
    // Создание объекта RecognitionAudio с содержимым аудиофайла
    $audio = (new RecognitionAudio())->setContent(file_get_contents($audioFile));
    // Создание объекта RecognitionConfig с конфигурацией распознавания
    $config = (new RecognitionConfig())
        ->setEncoding(RecognitionConfig\AudioEncoding::LINEAR16)
        ->setSampleRateHertz(16000)
        ->setLanguageCode('ru-RU');

    // Выполнение распознавания речи
    try {
        $response = $client->recognize($config, $audio);

        // Обработка результатов распознавания
        foreach ($response->getResults() as $result) {
            $alternative = $result->getAlternatives()[0];
            echo sprintf("Результат распознавания: %s\n", $alternative->getTranscript());
        }
    } catch (ApiException $e) {
        print_r(sprintf('Exception: %s', $e));
    }

    // Закрытие клиентского соединения
    $client->close();
} catch (ValidationException $e) {
    print_r(sprintf("Exception: %s\n", $e));
}
