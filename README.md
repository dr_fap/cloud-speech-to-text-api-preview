# Cloud Speech-to-Text API Preview

![Cloud Speech-to-Text](https://robotstart.info/wp-content/uploads/2018/04/Google-new-Cloud-Speech-to-Text.png)

## Начало

### Создание Google Cloud

- Сначала надо создать аккаунт Google Cloud и подключить это API: [**Cloud Speech-to-Text API**](https://console.cloud.google.com/apis/library/speech.googleapis.com)

![img.png](img.png)

- Затем создаете сервисный аккаунт здесь: [**Credentials**](https://console.cloud.google.com/apis/credentials) (Service account)

![img_1.png](img_1.png)

- Заходите во вкладку **KEYS** и скачивайте **JSON** файл:
  - **ADD KEY**
    - Key type: **JSON**
      - **Create**

      ![img_2.png](img_2.png)


### Клонирование репозитория

`git clone https://gitlab.com/dr_fap/cloud-speech-to-text-api-preview.git`

### Настройка окружения

- Перетащите файл `*.json` в папку `cloud-speech-to-text-api-preview/files/keys`
- Удалите `example.json`
- В файле `speech-to-text-preview.php` замените имя на своё
  - Поиск переменной можно осуществить по `CTRL + SHIFT + F` и введите `TODO: FILEKEY`
- Создайте папку `media` в папке `files`
  - Перетащите туда любой `*.wav` файл
  - Укажите его название по аналогии с `*.json`, только имя укажите вашего файла.
    - Поиск осуществляется по `TODO: FILENAME`

### Установите все зависимости

Используйте готовый `composer.json`

### Запустите скрипт